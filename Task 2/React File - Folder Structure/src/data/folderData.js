const explorer = {
    name: "My Computer",
    isFolder: true,
    items: [
        {
            name: "Documents",
            isFolder: true,
            items: [
                {
                    name: "Document1.jpg",
                    isFolder: false
                },
                {
                    name: "Document2.jpg",
                    isFolder: false
                },
                {
                    name: "Document3.jpg",
                    isFolder: false
                }
            ]
        },
        {
            name: "Desktop",
            isFolder: true,
            items: [
                {
                    name: "Screenshot1.jpg",
                    isFolder: false
                },
                {
                    name: "videopal.mp4",
                    isFolder: false
                }
            ]
        },
        {
            name: "Downloads",
            isFolder: true,
            items: [
                {
                    name: "Drivers",
                    isFolder: true,
                    items: [
                        {
                            name: "Printerdriver.dmg",
                            isFolder: false
                        },
                        {
                            name: "cameradriver.dmg",
                            isFolder: false
                        }
                    ]
                }
            ]
        },
        {
            name: "Applications",
            isFolder: true,
            items: [
                {
                    name: "Webstorm.dmg",
                    isFolder: false
                },
                {
                    name: "Pycharm.dmg",
                    isFolder: false
                },
                {
                    name: "FileZila.dmg",
                    isFolder: false
                },
                {
                    name: "Mattermost.dmg",
                    isFolder: false
                }
            ]
        },
        {
            name: "chromedriver.dmg",
            isFolder: false
        }
    ]
};

export default explorer;
