import { useState } from "react";
import { FaFolder } from "react-icons/fa";
import { FaFile } from "react-icons/fa";

function Folder({ explorer }) {
    const [expand, setExpand] = useState(false);

    // checking if the exlporer.isFolder is true
    if (explorer.isFolder) {
        return (
            <div>
                {/* adding the folder icon */}
                <FaFolder className="icon" />
                <span
                    style={{ paddingLeft: "3px", fontWeight: "bold", fontSize: "20px" }}
                    onClick={() => setExpand(!expand)}
                >
                    {explorer.name}
                    <br />
                </span>
                {/* if the expand is true it will be dislpayed else display will be none */}
                <div
                    style={{ display: expand ? "block" : "none", paddingLeft: "30px" }}
                >
                    {explorer.items.map((exp) => {
                        return (
                            <>
                                <Folder key={exp.name} explorer={exp} />
                            </>
                        );
                    })}
                </div>
            </div>
        );
    } else {
        return (
            <>
                <FaFile className="icon" />
                <span style={{paddingLeft:'3px', }}>
                    {explorer.name}
                    <br />
                </span>
            </>
        );
    }
}

export default Folder;
