window.addEventListener("scroll", function () { onScroll() });
window.addEventListener("DOMContentLoaded", function () { onScroll() });



function onScroll() {
    var images = document.querySelectorAll('.lazyload');
    for (var i = 0; i < images.length; i++) {
        var img = images[i]
        var rect = img.getBoundingClientRect();
        var isVisible = ((rect.top - window.innerHeight)<500 && (rect.bottom)> -50) ? true : false;

        if (isVisible) {
            if (!img.src) {
                img.src = img.dataset.src;
            }
        }
    }
}