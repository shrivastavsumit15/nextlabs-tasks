let slider = document.getElementById("myRange");
let output = document.getElementById("demo");
output.innerHTML = slider.value;

let card1 = document.getElementById('card1');
let card2 = document.getElementById('card2');
let card3 = document.getElementById('card3');


slider.oninput = function () {
    output.innerHTML = this.value;
    console.log(this.value);


    if(this.value <1){
        card1.style.transform ="scale(1)"
        card2.style.transform ="scale(1)"
        card3.style.transform ="scale(1)"

    }
    else if(this.value >=1 && this.value <=10){
        card1.style.transition ="all 0.5 ease-in-out"
        card2.style.transform ="scale(1)"
        card1.style.transform ="scale(1.15)"
    }
    else if (this.value > 10 && this.value <=20){
        card3.style.transform ="scale(1)"
        card1.style.transform ="scale(1)"
        card2.style.transition ="all 0.5 ease-in-out"
        card2.style.transform ="scale(1.15)"
    }
    else if(this.value> 20){
        card1.style.transform ="scale(1)"
        card2.style.transform ="scale(1)"
        card3.style.transition ="all 0.5 ease-in-out"
        card3.style.transform ="scale(1.15)"
    }
}